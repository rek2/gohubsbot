package gohubsbot

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"github.com/gorilla/websocket"
)

var ctx context.Context

func GoHubsBot(site, origin, goHubsName string) *websocket.Conn {

	o, err := url.Parse(origin)
	u, err := url.Parse(site)
	if err != nil {
		panic(err)
	}

	Header := http.Header{}
	Header.Set("Origen", o.Host)

	fmt.Printf("Connecting using origin: %s\n", o.Host)
	fmt.Printf("To Host: %s", u.Host)

	// sent url and pipe to websocket
	ws := websockets(u.Host, u.Path, u.RawQuery, goHubsName, Header)
	return ws
}
