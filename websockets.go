// File by ReK2
// hispagatos.org

package gohubsbot

import (
	"fmt"
	"log"
	"net/http"
	"net/url"

	"github.com/gorilla/websocket"
)


var room = `["1","1","ret","phx_join",{"hub_id":"zWXK8U6"}]`

var enterRoom1 = `["2","139","hub:zWXK8U6","events:entering",{}]`

var enterRoom3 = `["2","140","hub:zWXK8U6","events:entering_cancelled",{}]`
var enterRoom4 = `["2","36","hub:zWXK8U6","events:entered",{"isNewDaily":true,"isNewMonthly":true,"isNewDayWindow":true,"isNewMonthWindow":true,"initialOccupantCount":0,"entryDisplayType":"Screen","userAgent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36"}]`

var chikiBeat = `[null,"","phoenix","heartbeat",{}]`

func websockets(host, urlPath, query, goHubsName string, header http.Header) *websocket.Conn {

	enter := fmt.Sprintf(`["2","2","hub:zWXK8U6","phx_join",{"profile":{"avatarId":"PcJ8Sxb","displayName":"%s"},"push_subscription_endpoint":null,"auth_token":"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJyZXQiLCJleHAiOjE1OTgxOTc3NzgsImlhdCI6MTU5MDk0MDE3OCwiaXNzIjoicmV0IiwianRpIjoiYTM2ZTc4M2EtODkzMS00MDk3LWE2Y2UtODYwZDI0MWQ5NTEwIiwibmJmIjoxNTkwOTQwMTc3LCJzdWIiOiI3MTQ5OTU3NjQ1NTE0ODM4MTAiLCJ0eXAiOiJhY2Nlc3MifQ.NSGMMa0cLxTf4MKkOmBy9bUHz8P51Iha5iBl9BvEUaQGGAanRO5fquIcPkMbv0qzfJL5fZDUTm_61y-2UYhJ9g","perms_token":null,"context":{"mobile":false,"embed":false}}]`, goHubsName)
	enterRoom2 := fmt.Sprintf(`["2","32","hub:zWXK8U6","events:profile_updated",{"profile":{"avatarId":"PcJ8Sxb","displayName":"%s"}}]`, goHubsName)

	u := url.URL{Scheme: "wss", Host: host, Path: urlPath, RawQuery: query}
	log.Printf("connecting to %s", u.String())

	ws, _, err := websocket.DefaultDialer.Dial(u.String(), header)
	if err != nil {
		log.Fatal("dial:", err)
	}

	err = ws.WriteMessage(websocket.TextMessage, []byte(room))
	if err != nil {
		log.Fatal("something wrong room", err)
	}

	line, message := getMessage(ws)
	log.Printf("recv: %d, %s", line, message)

	err = ws.WriteMessage(websocket.TextMessage, []byte(enter))
	if err != nil {
		log.Fatal("something wrong enter", err)
	}

	line, message = getMessage(ws)
	log.Printf("recv: %d, %s", line, message)

	err = ws.WriteMessage(websocket.TextMessage, []byte(enterRoom1))
	if err != nil {
		log.Fatal("something wrong enterRoom1", err)
	}


	err = ws.WriteMessage(websocket.TextMessage, []byte(enterRoom2))
	if err != nil {
		log.Fatal("something wrong enterRoom2", err)
	}
	err = ws.WriteMessage(websocket.TextMessage, []byte(enterRoom4))
	if err != nil {
		log.Fatal("something wrong enterRoom4", err)
	}


	go Heartbeat(ws)
	return ws
}

func getMessage(ws *websocket.Conn) (int, []byte) {

	line, message1, err := ws.ReadMessage()
	if err != nil {
		log.Println("read: ", err)
	}
	return line, message1

}
