package gohubsbot

import (
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/id"
)

func MatrixSay(client *mautrix.Client, message string, channel id.RoomID) {

	client.SendText(channel, message)

}
