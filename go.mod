module gitlab.com/rek2/gohubsbot

go 1.14

require (
	github.com/gorilla/websocket v1.4.2
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	maunium.net/go/mautrix v0.7.0
)
