package gohubsbot

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"reflect"

	"github.com/gorilla/websocket"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/id"
)

// Map to allocate user ID and user Names

var usersMap = make(map[string]string)

// Json to Go

type PresenceDiff struct {
	Joins  map[string]*Presence // map to grab dynamic value
	Leaves map[string]*Presence // map to grab dynamic value
}

type Message struct {
	SessionID string `json:"session_id"`
	Body      string // we get the msg body
	Type      string
}

type PresenceState map[string]Presence

type Presence struct {
	Metas []*Meta
}

type Meta struct {
	Context     struct{}
	Permissions struct{}
	PhxRef      []byte `json:"phx_ref"`
	PhxRefPrev  []byte `json:"phx_ref_prev"`
	Presence    string `json:"presence"`
	Profile     Profile
	Roles       struct{}
}

type Profile struct {
	AvatarID    string
	DisplayName string
}

var messageTypes = map[string]reflect.Type{
	"presence_diff":  reflect.TypeOf(&PresenceDiff{}),
	"message":        reflect.TypeOf(&Message{}),
	"presence_state": reflect.TypeOf(&PresenceState{}),
}

// Decode Json from websocket to GO struts

func decode(data []byte) (interface{}, error) {
	var messageType string
	var raw json.RawMessage
	v := []interface{}{nil, nil, nil, &messageType, &raw}
	err := json.Unmarshal(data, &v)
	if err != nil {
		return nil, err
	}
	if len(raw) == 0 {
		return nil, errors.New("no message")
	}
	t := messageTypes[messageType]
	if t == nil {
		return nil, errors.New("unknown message type")
	}
	result := reflect.New(t.Elem()).Interface()
	err = json.Unmarshal(raw, result)
	return result, err
}

func WebsocketsListen(ws *websocket.Conn, matrixClient *mautrix.Client, matrixChannel id.RoomID, goHubsName string) {

	for {
		_, data, err := ws.ReadMessage()
		if err != nil {
			log.Printf("Read error: %v", err)
			break
		}

		v, err := decode(data)
		if err != nil {
			// turn on to debug or decode more json responses
			//log.Printf("Decode error: %v", err)
			continue
		}

		switch v := v.(type) {
		case *PresenceDiff:
			if len(v.Joins) != 0 {
				for a, p := range v.Joins {
					for _, m := range p.Metas {
						if m.Profile.DisplayName != goHubsName {
							msgBridge := fmt.Sprintf("%s: %s has Joined", m.Presence, m.Profile.DisplayName)
							// if user_id key is already there do not add it again
							if _, ok := usersMap[a]; !ok {
								usersMap[a] = m.Profile.DisplayName
							}
							fmt.Println(usersMap)
							go MatrixSay(matrixClient, msgBridge, matrixChannel)
						}

					}
				}
			}
			if len(v.Leaves) != 0 {
				for a, p := range v.Leaves {
					for _, m := range p.Metas {
						if m.Profile.DisplayName != goHubsName {
							msgBridge := fmt.Sprintf("%s: %s is leaving", m.Presence, m.Profile.DisplayName)
							//delete(usersMap, a) // find a good way to delete user_id number from hash
							fmt.Println(a)        // debug reason
							fmt.Println(usersMap) // debug reason
							go MatrixSay(matrixClient, msgBridge, matrixChannel)
						}
					}
				}
			}

		case *Message:
			userNick := usersMap[v.SessionID]
			body := fmt.Sprintf("%s says: %s", userNick, v.Body)
			go MatrixSay(matrixClient, body, matrixChannel)

		case *PresenceState:
			fmt.Println("Entering PresenceState")
			for n, p := range *v {
				for _, m := range p.Metas {
					fmt.Printf("ID %s NAME: %s\n", n, m.Profile.DisplayName)
					usersMap[n] = m.Profile.DisplayName
				}
			}

		default:
			fmt.Printf("type %T not handled\n", v)
		}
	}

}
