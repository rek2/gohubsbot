package gohubsbot

import (
	"log"
	"time"

	"github.com/gorilla/websocket"
)

func Heartbeat(ws *websocket.Conn) {

	var chikiBeat = `[null,"","phoenix","heartbeat",{}]`
	for {
		timer := time.After(time.Second * 10)
		<-timer
		err := ws.WriteMessage(websocket.TextMessage, []byte(chikiBeat))

		if err != nil {
			log.Fatal("something wrong with websocket Heartbeat", err)
		}
	}
}
