package gohubsbot

import (
	"fmt"
	"log"

	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/id"
)

func Matrix(url, user, password string, channel id.RoomID) *mautrix.Client {
	var accessToken string
	client, err := mautrix.NewClient(url, "", "")
	if err != nil {
		log.Fatal("failed to create mautrix client")
	}
	fmt.Println(client)
	resp, err := client.Login(&mautrix.ReqLogin{
		Type: "m.login.password",
		Identifier: mautrix.UserIdentifier{
			Type: "m.id.user",
			User: user,
		},
		Password: password,
	})
	if err != nil {
		log.Fatalf("Error credentials part %s", err)
	}
	accessToken = resp.AccessToken
	fmt.Printf("We got TOKEN: %s\n", accessToken)

	client.SetCredentials(resp.UserID, resp.AccessToken)

	client.SendText(channel, "GoBot ha conectado!")
	return client
}
