package main

import (
	"gitlab.com/rek2/gohubsbot"
	"maunium.net/go/mautrix/id"
)

func main() {

	var matrixChannel id.RoomID

	matrixUrl := "https://matrix.hispagatos.org"
	matrixUser := "@user:hispagatos.org"
	matrixPassword := "hereyoursecurepassword"
	matrixChannel = "!LDhhCvywRXmuddJugF:hispagatos.org"

	HubsWebSocketSite := "mystifying-artificer.reticulum.io/socket/websocket?vsn=2.0.0"
	origin := "https://hubs.mozilla.com"
	goHubsName := "GoBot"

	ws := gohubsbot.GoHubsBot(HubsWebSocketSite, origin, goHubsName)
	matrixClient := gohubsbot.Matrix(matrixUrl, matrixUser, matrixPassword, matrixChannel)
	gohubsbot.WebsocketsListen(ws, matrixClient, matrixChannel, goHubsName)
}
