# GoHubsBot

package to create bots for mozilla hubs rooms
and bridge mozilla hubs chat to matrix and viceversa

## open to git merge requests

- [x] Changed all the bot to use websockets instead of chromedp, so no resources problems anymore
- [x] listen() to listen for chat and do something with the text
- [ ] Add e2ee to matrix client
- [x] bridged mozilla hubs to matrix:
  - [x] chat
  - [x] Joins
  - [x] leaves
  - [x] hash presence_state joins and leaves to match id with nick
  - [x] use presence joins leaves hash to sent nick of chat msg to matrix
  - [ ] add and remove presence from hash to keep hash updated
- [X] Rename bot name mozilla hubs side
- [ ] Bridge matrix to Mozilla hubs
- [ ] Add non-bridge related bot things like
  - [ ] RSSfeeds
  - [ ] News
  - [ ] MOTD

